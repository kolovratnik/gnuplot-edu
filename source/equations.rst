Téma: grafické řešení rovnic
============================

Obecně lze říci, že gnuplot pro řešení rovnic není nijak zvlášť vybaven (neumí sám o sobě řešení najít a zvýraznit) a že toto téma je matematické, nikoli informatické. Vystačíme si tedy s obecným vykreslováním funkcí a ukážeme možnosti, jak řešit některé problémy, například zobrazení mřížky, zvýraznění průsečíku a jeho souřadnic. Upozorníme také na několik případů, na které si je třeba dát pozor (průsečík mimo graf či rovnice přímky rovnoběžné s osou y).

Jednoduchá rovnice s funkcí sinus a jednotková kružnice
-------------------------------------------------------

Jako první budeme řešit jednoduchou rovnici s funkcí sinus :math:`sin(x)=-\frac{1}{2}`. V jednom obrázku ukážeme řešení pomocí jednotkové kružnice i pomocí grafu funkce sinus.

Při řešení pomocí grafu funkce vykreslíme průběhy obou stran rovnosti a budeme hledat průsečíky. Jak již bylo uvedeno, gnuplot řešení najít neumí. Místo toho zvýrazníme známá analytická řešení. Z řešení :math:`x\in\left\{\frac{7\pi}{6}+2k\pi, \frac{11\pi}{6}+2k\pi|k\in\mathbb{Z}\right\}` se v grafu objevují tři, jelikož je vykreslen větší interval než :math:`2\pi`.

Na jednotkové kružnici zvýrazníme průsečíky s pravou stranou rovnice, čili konstantní funkcí :math:`-\frac{1}{2}`. Postupně pro každý z obou průsečíků nakreslíme průvodič jednotkové kružnice a obloukem zvýrazníme úhel, který rovnici řeší.

Na osy vyneseme vybrané hodnoty zajímavé pro goniometrické funkce. Aby v grafu bylo místo pro jednotkovou kružnici, nastavíme rozsah na ose x od -1 do :math:`2\pi`. Aby se podlouhlý graf neúmerně neroztáhl na výšku, nastavíme poměr délek stran, aby odpovídal poměru rozsahů na osách x a y.

Jednotkovou kružnici bychom mohli vykreslit nadvakrát jako horní a spodní oblouk příkazem "plot sqrt(1-x*x), -sqrt(1-x*x)". V popisu grafu níže ale využijeme parametrický režim gnuplotu. Výchozí jméno nezávislé proměnné v parametrickém řežimu je :math:`t`. V souladu s očekáváním graf popíšeme dvojicí funkcí proměnné :math:`t` - první pro souřadnici x a druhou pro souřadnici y. Pro vykreslení funkcí jako v neparametrickém režimu stačí pro souřadnici x použít :math:`t` a pro souřadnici y pak přímo kreslenou funkci.

K vyznačení průsečíků lze použít různé způsoby. Zde použijeme malé kruhy (puntíky) pro jejich výraznost a vizuální přitažlivost. V příštím příkladu ukážeme, jak průsečíky označit křížky. Do grafu lze kreslit šipky a další objekty (oblouky, čtverce, lomené čáry, elipsy a texty). Právě toho využijeme k dotvoření grafu. Objektům se přiřazují čísla (index), pomocí nichž s nimi lze manipulovat (překreslit či smazat). Vedle polohy či velikosti lze nastavit barvu vykreslení, výplně a styl čáry. Klíčová slova fillcolor, fillstyle, linecolor a linestyle se zkracují na fc, fs, lc a ls.

Nakonec ještě ke dvěma řešením v grafu přidáme jejich souřadnice. K tomu využijeme příkaz set label. Aby text nezasahoval do grafu konstantní funkce, posumene jej o trochu níže. Využijeme možnosti umístění textu vlevo i vpravo od referenčního bodu. Abychom nemuseli kvůli souřadnicím jednoho řešení rozšiřovat kreslící plochu, rozdělíme jej na dva řádky.

Popis grafu je parametrizován pravou stranou rovnice označenou y. Až na dvě vyjímky bude fungovat pro :math:`y\in[-1,1]`. Jednou vyjímkou jsou souřadnice průsečíků uvedené napevno pro y=-0,5, které nelze parametrizovat tak, aby souřadnice zůstaly uvedeny v násobcích π. Druhou vyjímkou je ručně doplněný průsečík pro y=-0.5 v záporné části grafu.

Popis grafu a graf rovnice
**************************

.. xtodo:: gnuplot
  :title: Vykreslený příklad: jednotková kružnice a funkce sin
  :width: 700

  unset border # nekreslit rámeček
  set zeroaxis linetype 1 linecolor rgb "black" # zobrazovat osy

  # zobrazení vybraných popisků na osách
  set xtics axis ("π/4" pi/4, "π/2" pi/2, "3π/4" 3*pi/4, "π" pi,\
                  "5π/4" 5*pi/4, "3π/2" 3*pi/2, "7π/4" 7*pi/4, "2π" 2*pi)
  set ytics axis (-1, -0.5, 0, 0.5, 1)

  set xrange [-1:2*pi] # nastavení rozsahu na ose x
  set parametric # zapnutí parametrického režimu
  set size ratio 2.0/(2*pi+1) # nastavení poměru výšky a šírky grafu

  set trange [-1:2*pi] # nastavení intervalu hodnot parametru t

  y = -0.5 # hodnota pravé strany rovnice
  # vypočtení řešení x0 a x1 z [0,2π]
  if (y >= 0) {
      x0 = asin(y)
      x1 = pi - asin(y)
  } else {
      x0 = 1 * pi - asin(y)
      x1 = 2 * pi + asin(y)
  }

  # vyznačení průsečíků v grafu funkce sin(x)
  set object 1 circle at x0, y size 0.03 front fc rgb "red" fillstyle solid 1.0
  set object 2 circle at x1, y size 0.03 front fc rgb "red" fillstyle solid 1.0
  # ručně doplněný průsečík pro y=-0.5 v záporné části grafu
  set object 3 circle at asin(y), y size 0.03 front fc rgb "red" fillstyle solid 1.0

  # úhlové řešení na jednotkové kružnici
  set arrow from 0,0 to cos(x0), y nohead ls 10 lc rgb "#ffaa70"
  set arrow from 0,0 to cos(x1), y nohead ls 10 lc rgb "#ffaa70"
  set object 10 circle at 0,0 radius 0.25 arc[0:x0/(2*pi)*360] fs empty fc rgb "#ffaa70"
  set object 11 circle at 0,0 radius 0.35 arc[0:x1/(2*pi)*360] fs empty fc rgb "#ffaa70"

  # vyznačení průsečíků na jednotkové kružnici
  set object 4 circle at cos(x0), y size 0.03 front fc rgb "#ffaa70" fillstyle solid 1.0
  set object 5 circle at cos(x1), y size 0.03 front fc rgb "#ffaa70" fillstyle solid 1.0

  # souřadnice průsečíků pro y=-0.5
  ly = 0.15
  set label "[7π/6; -0.5]" at pi*7/6,-0.5 - ly right
  set label "[11π/6;\n-0.5]" at pi*11/6,-0.5 - ly

  plot cos(t), sin(t) notitle, \
       [0:2*pi] t, sin(t) title "sin(x)", \
       [-1:2*pi] t,y title sprintf("%g", y)

Tip na cvičení
**************

Příklad je poměrně rozsáhlý. V hodině by bylo praktické jej rozdělit a výsledek utvářet postupně. Cvičením pro samostatnou práci by mohlo být nechat žáky upravit příklad pro jinou goniometrickou funkci či jinou hodnotu, upravovat vizuální vlastnosti či některé části grafu vynechat.

.. note:: V příkazu plot je nastaveno, že křivka (funkce sinus) t, sin(t) se má vykreslit pouze na intervalu [0,2π]. Přesto se vykresluje již od -1. Využili jsme toho k zvýraznění dalšího řešení rovnice.

  Následující příklad ukazuje, jak omezení pro jednotlivé funkce funguje v běžném (neparametrickém) řežimu.

.. xtodo:: gnuplot
  :title: Vykreslený příklad: omezení vykreslovaných intervalů

  set key outside
  plot x, [0:5] x-1, [-3:3] x-2, x-3

Soustava dvou lineárních rovnic
-------------------------------

Další postupy ukážeme na grafickém řešení soustavy dvou lineárních rovnic o dvou neznámých. Vytvoříme přehledový obrázek se čtyřmi grafy zobrazujícími všechny možnosti, jak může soustava vypadat. Ukážeme další způsob zvýraznění řešení, jak zobrazit mřížku a jak vykreslit více grafů do jednoho obrázku.

Zobrazení více grafů do jednoho obrázku se nastavuje příkazem set multiplot. Celek můžeme pojmenovat volbou title, velikost tabulky, do které se budou umísťovat později grafy, uvedeme volbou layout. Nyní každý příkaz plot obsadí jednu pozici. Pro přehlednost začínáme popis každého grafu nastavením jeho nadpisu a jednotlivé grafy od sebe oddělujeme dvěma prázdnými řádky.

Aby bylo odečítání souřadnic řešení snazší, necháme příkazem set grid zobrazit mřížku. Volbou back se mřížka zobrazí v pozadí, aby nepřekreslovala legendu.

Právě jedno řešení
******************

Začneme nejčastějším případem, kdy je řešením jen jeden bod. Zadání soustavy je :math:`y=-x-3`, :math:`y=-2*x+5`. Nejprve jen prostě zobrazíme obě lineární funkce bez dalšího. Program gnuplot sám každou funkci vykreslí jiným stylem (jinou barvou), takže graf zůstává přehledý. Máme štěstí, při výchozím nastavení rozsahu na ose :math:`x\in[-10,10]` se přímky v grafu protínají v bodě :math:`[8,-11]`. V dalším grafu si ukážeme, jak tento bod vyznačit.

Detail řešení
*************

Druhou pozici obsadíme detailem rešení téže soustavy. Do příkazu plot ke dvěma lineárním funkcím přidáme zobrazení jednoho datového bodu - řešení. Místo názvu souboru uvedeme '-' a souřadnice bodu zapíšeme přímo do popisu grafu za příkaz plot (anglicky je tato konstrukce známa jako `Here document`_) a ukončíme je písmenem e na začátku řádku. Volbou pointtype nastavíme, že bod bude vyznačem křížkem. Volba notitle zajistí, že se tento datový soubor neobjeví v legendě.

.. _Here document: https://en.wikipedia.org/wiki/Here_document

Program gnuplot zaměří graf na datový bod automatickým nastavením rozsahů na osách. Pak patrně selže automatické nastavení popisků na osách. Na ose x se nastaví krok 0,02, popisků je moc a překrývají se. Na ose y se nastaví krok 0,05, popisky jsou zbytečně hustě. Proto ručně určíme krok a formát popisků příkazem set [xy]tics. Formát bylo nutné nastavit, aby se všechny popisky zobrazily na týž počet desetinných míst. Celá čísla se jinak zobrazovala bez desetinné části, což při zarovnání doprava působilo neesteticky.

Řešení neexistuje
*****************

Další variantou je situace, kdy zadané lineární rovnice nemají žádné řešení. V grafu tak vidíme dvě rovnoběžné přímky. Zadání soustavy je :math:`y=-x-3`, :math:`y=-x+5`. Prosté vykreslení funkcí příkazem plot vede k uspokojivému výsledku. Jen se musíme vypořádat s tím, že pro "detail řešení" jsme změnili nastavení popisků na osách. Vrátíme se k automatickému kroku a výchozímu formátovacímu řetězci. Ten jsme zjistili příkazem show xtics.

Nekonečně mnoho řešení
**********************

Zbývá případ, kdy jsou rovnice lineárně závislé a soustava má nekonečně mnoho řešení. Vzhledem k požadavku na tvar rovnic (osamostatněmá proměnná y na jedné straně, posčítané koeficienty u lineárního členu a sečtené konstanty) musí být rovnice totožné. I kdybychom tyto požadavky uvolnili, přímky rovnicemi popsané budou totožné. Je třeba nějak zařídit, aby byly v grafu obě patrné. Kdybychom je bez dalšího vykresili příkazem plot, druhá přímka by překryla první, první by nebyla vidět.

Jednou možností je vykreslit první přímku silnější čárkou, druhou slabší. Řešíme soustavu :math:`y=-x-3`, :math:`y=-x-3`. Pro ni by kreslící příkaz s různě silnými přímkami mohl vypadat takto: plot -x-3 lw 7, -x-3 lw 3. V přehledovém grafu níže předvedeme jinou možnost. Program gnuplot umožňuje kreslit různými, dokonce uživatelsky definovatelnými, druhy čar. První přímku tedy vykreslíme plnou čarou jako obvykle a druhou přímku čárkovanou čarou, což zajistí, že pod ní ta první bude vidět. I když by to mělo stačit, přímky v obrázku byly špatně rozlišitelné. Osvědčilo se zobrazit obě přímky jako čárkované s jinou délkou čárky a mezery. Z botahých možností popisu druhu čáry využíváme určení délky čáry a délky mezery dvojicí čísel v závorkách za volbou dashtype.


Popis grafů a grafy soustav rovnic
**********************************

.. xtodo:: gnuplot
  :title: Vykreslený příklad: více grafů v jednom obrázku
  :width: 700

  set zeroaxis linetype 1 linecolor rgb "black" # zobrazovat osy
  set multiplot title "Rozbor řešení soustavy 2 lineárních rovnic o 2 neznámých" \
      layout 2,2 # kreslit 2 řádky a 2 sloupce grafů do jednoho obrázku

  set grid back lc rgb'#808080' lt 0 lw 1 # mřížka


  set title "Právě jedno řešení"
  plot -x-3, -2*x+5


  set title "Detail řešení"
  set xtics 0.1 format "%0.1f" # nastavení kroku a formátu popisků osy x
  set ytics 0.2 format "%0.1f" # a osy y
  plot -x-3, -2*x+5, '-' with points pointtype 2 linecolor rgb "red" notitle
  8 -11
  e


  set title "Žádné řešení"
  set xtics autofreq format "% h" # návrat k výchozímu nastavení popisků na osách
  set ytics autofreq format "% h"
  plot -x-3, -x+5


  set title "Nekonečně mnoho řešení"
  plot -x-3 dashtype (20,20) lw 2, \
       -x-3 dashtype (10,10) lw 2


Skoro rovnoběžné grafy
**********************

Může se stát, že se přímky na grafu neprotnou jako v následujícím příkladě.

.. xtodo:: gnuplot
  :title: Vykreslený příklad: přímky svírající malý úhel

   plot x-3, 1.1*x+5

Může to znamenat, že soustava rovnic nemá řešení. To jsme viděli v rozboru výše. Může to ale také znamenat, že průsečík leží mimo zobrazenou část kartézské soustavy souřadnic, což se stalo v našem příkladě. Lze tak žákům předvést, že grafické řešení má své limity a nelze se na něj slepě spoléhat. Ani v případě s rovnicí s funkcí sinus nám grafická metoda neukázala všechna řešení, museli jsme vědět, že jich je nekonečně mnoho a řešení správně zapsat.

Svislá přímka
*************

Zadání soustavy by mohlo obsahovat rovnici ve tvaru :math:`0y=ax+b`, tedy :math:`x=-\frac{b}{a}`, kdy koeficient u y je roven 0. Taková rovnice odpovídá přímce rovnoběžné s osou y. Pokud bychom měli takovou přímku zobrazit, přepneme gnuplot do parametrického režimu a přímky vykreslíme v něm. Příkazem set dummy nastavíme jméno nezávislé proměnné zpět na x (přepnutím do parametrického režimu se nezávislá proměnná pojmenuje t a přenastaví se výchozí rozsah na ose x). Následuje zobrazení soustavy :math:`x=2`, :math:`y=x-3`.

.. xtodo:: gnuplot
  :title: Vykreslený příklad: svislá přímka v parametrickém režimu

  set parametric # zapnutí parametrického režimu
  set dummy x # jméno nezávislé promenné
  plot 2, x title "x=2", x, x-3 title "y=x-3"

.. note:: V příkladech jsme předpokládali, že soustava rovnic je v normalizovaném tvaru funkcí přímek :math:`y=a_1x+b_1`, :math:`y=a_2x+b_2`. Podobný materiál pro rozvinutí příkladů by poskytla jedna rovnice o jedné neznámé ve tvaru :math:`a_1x+b_1=a_2x+b_2`. Levou stranu bychom vykreslovali jako jednu přímku, pravou jako druhou. Souřadnice x průsečíku je řešením rovnice, souřadnice y je hodnotou, kterou obě strany rovnice shodně v bodě x nabývají.

Tipy na cvičení
***************

Příklad lze variovat - řešit jiné soustavy, vykreslit různé případy samostatně, hrát si se vzhledem, například zvolit kontrastnější barvy, aby překrývající se přímky byly lépe vidět. Obdobně by bylo možné provést rozbor kvadratické rovnice.
