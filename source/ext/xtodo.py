# -*- coding: utf-8 -*-
"""
    sphinx.ext.xtodo
    ~~~~~~~~~~~~~~~

    Allow xtodos to be inserted into your documentation.  Inclusion of xtodos can
    be switched of by a configuration variable.  The xtodolist directive collects
    all xtodos of your project and lists them along with a backlink to the
    original location.

    :copyright: Copyright 2007-2018 by the Sphinx team, see AUTHORS.
    :license: BSD, see LICENSE for details.
"""

from typing import Any

from docutils import nodes, statemachine
from docutils.parsers.rst import directives

import sphinx
from sphinx.locale import _
from sphinx.errors import NoUri
from sphinx.util import logging
from sphinx.util.nodes import set_source_info
from sphinx.util.texescape import tex_escape_map
from docutils.parsers.rst import Directive
from docutils.parsers.rst.roles import set_classes
from docutils.parsers.rst.directives.admonitions import BaseAdmonition
from docutils.parsers.rst.directives.images import Image, Figure
from docutils.parsers.rst.directives.body import CodeBlock

#from sphinx.transforms import SphinxTransform
from sphinx.transforms.post_transforms.images import BaseImageConverter
from sphinx.util.images import get_image_extension
from sphinx.util.osutil import ensuredir
from hashlib import sha1
import os, math, tempfile, subprocess
import shutil

if False:
    # For type annotation
    from typing import Any, Dict, Iterable, List  # NOQA
    from sphinx.application import Sphinx  # NOQA
    from sphinx.environment import BuildEnvironment  # NOQA

logger = logging.getLogger(__name__)

_app = None

def builder_inited(app):
  global _app
  _app = app
  for x in _app.config:
    print(x)

def build_finished(app, exception):
  global _app
  _app = None


class xtodo_node(nodes.Admonition, nodes.Element):
    pass


class xtodolist(nodes.General, nodes.Element):
    pass

class imageplaceholder(nodes.Part, nodes.Element):
    pass

class XTodo(Directive):
    optional_arguments = 1
    has_content = True
    option_spec = {
        'title': directives.unchanged,
    }
    option_spec.update(Figure.option_spec)
    option_spec.update(CodeBlock.option_spec)

    def run(self):
        print(('name', self.name))
        print(('content', self.content))
        print(('options', self.options))
        print(('arguments', self.arguments))
        self.assert_has_content()
        if self.arguments:
            language = self.arguments[0]
        else:
            language = ''
        set_classes(self.options)
        classes = ['code']

        # Directive:
        #     def __init__(self, name, arguments, options, content, lineno,
        #                  content_offset, block_text, state, state_machine):


        image_options = { k:v for k, v in self.options.items() if k in Figure.option_spec }
        code_options = { k:v for k, v in self.options.items() if k in CodeBlock.option_spec }
        figure_content = statemachine.StringList([self.options['title']] if 'title' in self.options else [])
        (figure, ) = Figure('Figure', ['img.gnuplot'], image_options, figure_content, self.lineno, 1, '', self.state, self.state_machine).run()
        rendered = imageplaceholder()
        figure._render_image_properties = {
            'name': self.name,
            'lineno': self.lineno,
            'arguments': self.arguments,
            'options': self.options,
            'content': self.content,
            'content_offset': self.content_offset,
            'block_text': self.block_text,
        }

        (code, ) = CodeBlock('CodeBlock', ['gnuplot'], code_options, self.content, self.lineno, 0, '', self.state, self.state_machine).run()


        node = nodes.compound('\n'.join(self.content), classes=classes)
        # node += nodes.literal_block('\n'.join(self.content), classes=classes)
        # node += nodes.Text('ahoj', 'ahoj')
        # node += caption
        node += code
        node += figure
        # node += rendered
        # figure_node = nodes.figure('', image_node)
        # self.add_name(node)
        return [node]

def process_xtodos(app, doctree):
    # type: (Sphinx, nodes.Node) -> None
    # collect all xtodos in the environment
    # this is not done in the directive itself because it some transformations
    # must have already been run, e.g. substitutions
    env = app.builder.env
    if not hasattr(env, 'xtodo_all_xtodos'):
        env.xtodo_all_xtodos = []  # type: ignore
    for node in doctree.traverse(xtodo_node):
        app.emit('xtodo-defined', node)

        try:
            targetnode = node.parent[node.parent.index(node) - 1]
            if not isinstance(targetnode, nodes.target):
                raise IndexError
        except IndexError:
            targetnode = None
        newnode = node.deepcopy()
        del newnode['ids']
        env.xtodo_all_xtodos.append({  # type: ignore
            'docname': env.docname,
            'source': node.source or env.doc2path(env.docname),
            'lineno': node.line,
            'xtodo': newnode,
            'target': targetnode,
        })

        if env.config.xtodo_emit_warnings:
            logger.warning("TODO entry found: %s", node[1].astext(),
                           location=node)
    '''
    for node in doctree.traverse(imageplaceholder):
        props = node._render_image_properties
        (img, ) = Image('Image', ['img.jpg'], {}, [], props['lineno'], props['content_offset'], '', 0, None).run()

        index = node.parent.index(node)
        node.parent[index] = img
    '''



class XTodoList(Directive):
    """
    A list of all xtodo entries.
    """

    has_content = False
    required_arguments = 0
    optional_arguments = 0
    final_argument_whitespace = False
    option_spec = {}  # type: Dict

    def run(self):
        # type: () -> List[xtodolist]
        # Simply insert an empty xtodolist node which will be replaced later
        # when process_xtodo_nodes is called
        return [xtodolist('')]


def process_xtodo_nodes(app, doctree, fromdocname):
    # type: (Sphinx, nodes.Node, unicode) -> None
    if not app.config['xtodo_include_xtodos']:
        for node in doctree.traverse(xtodo_node):
            node.parent.remove(node)

    # Replace all xtodolist nodes with a list of the collected xtodos.
    # Augment each xtodo with a backlink to the original location.
    env = app.builder.env

    if not hasattr(env, 'xtodo_all_xtodos'):
        env.xtodo_all_xtodos = []  # type: ignore

    for node in doctree.traverse(xtodolist):
        if node.get('ids'):
            content = [nodes.target()]
        else:
            content = []

        if not app.config['xtodo_include_xtodos']:
            node.replace_self(content)
            continue

        for xtodo_info in env.xtodo_all_xtodos:  # type: ignore
            para = nodes.paragraph(classes=['xtodo-source'])
            if app.config['xtodo_link_only']:
                description = _('<<original entry>>')
            else:
                description = (
                    _('(The <<original entry>> is located in %s, line %d.)') %
                    (xtodo_info['source'], xtodo_info['lineno'])
                )
            desc1 = description[:description.find('<<')]
            desc2 = description[description.find('>>') + 2:]
            para += nodes.Text(desc1, desc1)

            # Create a reference
            newnode = nodes.reference('', '', internal=True)
            innernode = nodes.emphasis(_('original entry'), _('original entry'))
            try:
                newnode['refuri'] = app.builder.get_relative_uri(
                    fromdocname, xtodo_info['docname'])
                newnode['refuri'] += '#' + xtodo_info['target']['refid']
            except NoUri:
                # ignore if no URI can be determined, e.g. for LaTeX output
                pass
            newnode.append(innernode)
            para += newnode
            para += nodes.Text(desc2, desc2)

            xtodo_entry = xtodo_info['xtodo']
            # Remove targetref from the (copied) node to avoid emitting a
            # duplicate label of the original entry when we walk this node.
            if 'targetref' in xtodo_entry:
                del xtodo_entry['targetref']

            # (Recursively) resolve references in the xtodo content
            env.resolve_references(xtodo_entry, xtodo_info['docname'],
                                   app.builder)

            # Insert into the xtodolist
            content.append(xtodo_entry)
            content.append(para)

        node.replace_self(content)


def purge_xtodos(app, env, docname):
    # type: (Sphinx, BuildEnvironment, unicode) -> None
    if not hasattr(env, 'xtodo_all_xtodos'):
        return
    env.xtodo_all_xtodos = [xtodo for xtodo in env.xtodo_all_xtodos  # type: ignore
                          if xtodo['docname'] != docname]


def merge_info(app, env, docnames, other):
    # type: (Sphinx, BuildEnvironment, Iterable[unicode], BuildEnvironment) -> None
    if not hasattr(other, 'xtodo_all_xtodos'):
        return
    if not hasattr(env, 'xtodo_all_xtodos'):
        env.xtodo_all_xtodos = []  # type: ignore
    env.xtodo_all_xtodos.extend(other.xtodo_all_xtodos)  # type: ignore


def visit_xtodo_node(self, node):
    # type: (nodes.NodeVisitor, xtodo_node) -> None
    self.visit_admonition(node)
    # self.visit_admonition(node, 'xtodo')


def depart_xtodo_node(self, node):
    # type: (nodes.NodeVisitor, xtodo_node) -> None
    self.depart_admonition(node)


def latex_visit_xtodo_node(self, node):
    # type: (nodes.NodeVisitor, xtodo_node) -> None
    title = node.pop(0).astext().translate(tex_escape_map)
    self.body.append(u'\n\\begin{sphinxadmonition}{note}{')
    # If this is the original xtodo node, emit a label that will be referenced by
    # a hyperref in the xtodolist.
    target = node.get('targetref')
    if target is not None:
        self.body.append(u'\\label{%s}' % target)
    self.body.append('%s:}' % title)


def latex_depart_xtodo_node(self, node):
    # type: (nodes.NodeVisitor, xtodo_node) -> None
    self.body.append('\\end{sphinxadmonition}\n')

class GnuplotImageTransform(BaseImageConverter):

    terminals = {
        'application/pdf': 'pdfcairo',
        'image/png': 'png',
    }

    default_priority = 100

    def apply(self, **kwargs: Any) -> None:
        # BaseImageConverter.apply traverses nodes.image
        for node in self.document.traverse(nodes.figure):
            if self.match(node):
                self.handle(node)

    def match(self, node):
        return hasattr(node, '_render_image_properties') and node.children[0]['uri'].endswith('.gnuplot')

    def handle(self, node):
        node.children[0]._render_image_properties = node._render_image_properties
        node = node.children[0]

        code = list(node._render_image_properties['content'])
        user_code = '\n'.join(code)
        basename = self.basename(user_code)
        dirname = 'gpimg'
        mimetype = 'image/png'
        ext = get_image_extension(mimetype)
        print('self.imagedir', self.imagedir)
        path = os.path.join(self.imagedir, dirname)
        ensuredir(path)
        path = os.path.join(path, basename + ext)

        with tempfile.TemporaryDirectory() as tmpdirname:
            generated_path = self.generate_img(tmpdirname, mimetype, node, code)
            shutil.move(generated_path, path)
        # self.app.env.original_image_uri.pop(node['uri'])
        # self.app.env.original_image_uri[newpath] = path

        print('path', path)
        print('CWD',  os.getcwd())
        print('self.app.doctreedir',  self.app.doctreedir)

        path = '../' + os.path.relpath(path, os.getcwd())

        print('path', path)
        print('self.app.builder.supported_image_types', self.app.builder.supported_image_types)
        # self.app.builder.supported_image_types ['image/svg+xml', 'image/png', 'image/gif', 'image/jpeg']
        # self.app.builder.supported_image_types ['application/pdf', 'image/png', 'image/jpeg']

        if 'candidates' in node:
            node['candidates'].pop('?')
        else:
            node['candidates'] = {}
        node['candidates'][mimetype] = path
        node['uri'] = path

    def generate_img(self, tmpdirname, mimetype, node, code):
        src_file = os.path.join(tmpdirname, 'source.gnuplot')
        img_file = os.path.join(tmpdirname, 'output')
        options = node._render_image_properties['options']
        width = int(options.get('width', 500))
        try:
            height = int(options['height'])
        except KeyError:
            height = 0.5 + 2 * width / (1 + math.sqrt(5))
        terminal = 'set terminal %s size %d,%d' % (self.terminals[mimetype], width, height)
        output = 'set output \'%s\'' % img_file
        x = [ terminal, output ] # escaping of img_file?
        y = []
        source_code = x + code + y
        with open(src_file, 'w') as fh:
            fh.write('\n'.join(source_code))
        cmd = [ self.config.gnuplot, src_file ]
        print('GNUPLOT COMMAND: %s' % ' '.join(cmd))
        print('Source code')
        print('===========')
        print('\n'.join(source_code))
        print('===========')
        ret = subprocess.call(cmd, stdin=subprocess.PIPE)
        if ret is not 0:
            # error
            print('chyba')
            return None
        print('EXIT CODE: %d' % ret)
        print('===========')
        os.unlink(src_file)
        return img_file

    def basename(self, content):
        return sha1(content.encode("utf-8")).hexdigest()



def setup(app):
    # type: (Sphinx) -> Dict[unicode, Any]
    app.add_event('xtodo-defined')
    app.add_config_value('xtodo_include_xtodos', False, 'html')
    app.add_config_value('xtodo_link_only', False, 'html')
    app.add_config_value('xtodo_emit_warnings', False, 'html')
    app.add_config_value('gnuplot', 'gnuplot', 'env')

    app.add_node(xtodolist)
    app.add_node(xtodo_node,
                 html=(visit_xtodo_node, depart_xtodo_node),
                 latex=(latex_visit_xtodo_node, latex_depart_xtodo_node),
                 text=(visit_xtodo_node, depart_xtodo_node),
                 man=(visit_xtodo_node, depart_xtodo_node),
                 texinfo=(visit_xtodo_node, depart_xtodo_node))

    app.add_directive('xtodo', XTodo)
    app.add_directive('xtodolist', XTodoList)
    app.connect('doctree-read', process_xtodos)
    app.connect('doctree-resolved', process_xtodo_nodes)
    app.connect('env-purge-doc', purge_xtodos)
    app.connect('env-merge-info', merge_info)
    app.connect('builder-inited', builder_inited)
    app.connect('build-finished', build_finished)
    app.add_transform(GnuplotImageTransform)
    return {'version': sphinx.__display_version__, 'parallel_read_safe': True}
