.. gnuplot documentation master file, created by
   sphinx-quickstart on Tue Sep 20 10:39:41 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Úvod do tvorby grafů pomocí programu gnuplot v příkladech
=========================================================

Obsah:

.. toctree::
   :maxdepth: 2

   introduction.rst

   about.rst

   help.rst

   topics.rst

   example1.rst

   dataplot.rst

   equations.rst

   conclusion.rst
