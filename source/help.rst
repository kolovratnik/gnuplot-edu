Základy ovládání
================

.. highlight:: gnuplot

Ještě než se pustíme do vykreslování grafů, seznámíme se se způsoby, jak gnuplot ovládat, jakým způsoben připravit vstup, čili popis grafu, a jak získat nápovědu.

Vstup ze souboru
----------------

Jednou možností je připravit popis grafu do souboru. Program gnuplot zpracuje soubory zadané na příkazové řádce v uvedeném pořadí. Mezi jednotlivými soubory se stav gnuplotu neresetuje. To znamená, pokud bychom například v prvním vstupním souboru nastavili rozsah hodnot na ose x, bude tento rozsah platit i pro druhý soubor, tedy dokud nebude změněn.

Výhodou je, že prvním vstupním souborem lze gnuplot nastavit a dalšími soubory generovat grafy. Tytéž soubory mohou vykreslit grafy v jiném stylu, podle toho, co obsahoval první soubor.

Při čtení vstupu ze souboru gnuplot vykoná v něm uvedené příkazy a skončí. Což je v pořádku, pokud výstupy směřovaly do souboru. Měl-li však být vykreslen graf na obrazovku, okno problikne a hned zmizí. Abychom si jej stihli prohlédnout, lze použít příkaz pause <čas>. Je-li čas kladný, čeká daný počet sekund, je-li -1 čeká na stisk klávesy enter v terminálu. Jinou možností je spustit gnuplot s volbou -p. Po zpracování vstupu zůstanou okna otevřená (byla-li nějaká). Výhodou je, že se nemusí vkládat příkaz pause do vstupního souboru. Ovšem k prohlédnutí zůstane pouze poslední vykreslený graf v každém okně.

Je ještě další možnost, jak okno udržet otevřené, a to volbou persist v příkazu set terminal.

Interaktivní ovládání
---------------------

Spustíme-li gnuplot bez určení vstupního souboru, nastartuje se v interaktivním režimu. To znamená, že zpracovává příkazy zadávané na vnitřní příkazové řádce. Ta bývá docela uživatelsky přátelská. Během editace se po řádce lze pohybovat šipkami doleva a doprava, šipkami nahoru a dolů lze procházet minulé příkazy (ty se dokonce ukládají, aby byly k dispozici při příštím spuštění) a pomocí klávesové zkratky Ctrl-R lze v historii interaktivně hledat příkaz shodující se s následně zadanými znaky.

Režim vstupu
------------

Oba režimy vstupu mají své užití. Vstup ze souboru se hodí pro dávkové zpracování nebo delší skripty. Na příkazové řádce naopak snáze vyzkoušíme nějakou konstrukci, dobře poslouží i k vykreslení jednoduchého grafu.

Vestavěná nápověda
------------------

V interaktivním řežimu je k dispozici vestavěná nápověda. Vyvolat ji lze příkazem

.. code::

  help {<topic>}

Zápis ve špičatých závorkách značí, že se očekává doplnění nějaké hodnoty, slovo napovídá, co hodnota určuje. Složené závorky označují volitelnou část. Lze tedy zadat pouze příkaz `help` nebo se zeptat třeba na příkaz plot pomocí `help plot`.

V notaci nápovědy se objevuje ještě svislá čára označující volbu jedné možnosti. V nápovědě ke stylu `help style` se objevuje zápis

.. code::

  {linecolor | lc <colorspec>}

značící, že můžeme určit barvu čáry klíčovým slovem linecolor nebo jeho zkratkou lc, jehož argumentem je požadované barva. Slovo ve špičatých závorkách je název argumentu a napovídá, jaký typ hodnoty se očekává. Je-li v uvozovkách, očekává se řetězec.

.. code::

  set title {"<title-text>"}
