Úvod
====

Gnuplot je program pro tvorbu grafů, tedy zobrazení průběhů matematických funkcí a souborů dat, měření, pozorování. Vstupem je textový popis grafu a případný soubor dat, výstupem je graf buď interaktivně zobrazený v okně na obrazovce počítače nebo uložený do souboru jako vektorový či rastrový obrázek nebo popis grafu třeba v Latexu, aby mohl být snadno vložen do dokumentu psaném v tomto jazyce. Nově lze vynegerovat stránku s Javascriptovým programem, který graf vykreslí v prvku HTML5 canvas.

V textovém popisu grafu lze zvolit druh grafu a nastavit mnoho jeho vlastností, vytvořit legendu, zvolit výstupní formát, definovat název grafu, rozsahy na osách apod.

Následuje motivace, proč uvažovat nad začleněním programu gnuplot do výuky informatiky. Vynášení naměřených, pozorovaných či spočtených hodnot a vykreslování průběhů funkcí se objevuje v matematice, ve fyzice, v odborných předmětech, při tvorbě protokolů z laboratorních experimentů. Někdy se vyžaduje zpracování protokolů na počítači. Tvorba grafů v tabulkových kalkulátorech bývá součástí výuky informatiky.

Zařazení gnuplotu do výuky dává možnost ukázat jiný způsob práce s počítačem, než je interaktivní ovládání programu s grafickým uživatelských rozhraním. Textový popis grafu může být opakovaně použit k zobrazení různých dat, poslouží jako šablona. Umožňuje automatizaci zpracování dat, zpracování v dávkách. Mohlo by posloužit jako předstupěň k programování. Konstrukce jsou jednoduché, poměrně krátké, ale vyžadují rigidní zápis vyhovující gramatice jazyka gnuplotu.
