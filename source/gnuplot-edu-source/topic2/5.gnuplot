set key off # nezobrazovat legendu
set sample 12 # funkci vzorkovat ve 12 bodech
set boxwidth 0.5 # nastavení šířky sloupce
set style fill solid # vyplnění sloupců barvou
set xtics 1 # popisky na ose x mají krok 1
days(month) = 31 - month % 7 % 2 - 2 * (month == 1)
plot [0.25:12.75] 0, [1:12] days(int(x) - 1) title "dny" with boxes