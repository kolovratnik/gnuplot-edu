set title "Počet dní v měsíci"
set boxwidth 0.5 # nastavení šířky sloupce
set style fill solid # vyplnění sloupců barvou
set xrange [0.25:12.75] # interval na ose x
set yrange [27:31.9] # interval na ose y
set xtics 1 # popisky na ose x mají krok 1
set ytics 1 # popisky na ose y mají krok 1
plot "source/month.txt" using ($0+1):1 title "dny" with boxes