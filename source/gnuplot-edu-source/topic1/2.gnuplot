#!/usr/bin/gnuplot

# set encoding utf8
set key off # nezobrazovat legendu
set xrange [0:2*pi] # interval na ose x
set title "Graf funkce sin(x), x∈[0,2π]"
set zeroaxis linetype 1 linecolor rgb "black" # zobrazovat osy
# šipka na konci osy x
set arrow from 2*pi-0.1,0 rto 0.4,0 head size 0.2,25 # délka šipky 0.2, úhel 25°
set label "x" at 2*pi+0.4, -0.15 # jméno osy x a jeho umístění
# šipka na konci osy y
set arrow from 0,1 rto 0,0.3 head size 0.2,25 # délka šipky 0.2, úhel 25°
set label "y" at -0.3, 1.25 # jméno osy y a jeho umístění
set rmargin 2*pi+1 # udělat místo pro popisek osy x
unset border # nekreslit rámeček
set xtics axis ("π/6" pi/6, "π/4" pi/4, "π/2" pi/2, "3π/4" 3*pi/4, "π" pi,\
                "5π/4" 5*pi/4, "3π/2" 3*pi/2, "7π/4" 7*pi/4, "2π" 2*pi)
set ytics axis (-1, "-√3/2" -sqrt(3)/2, "-√2/2" -sqrt(2)/2, -0.5,\
                 0, 0.5, "√2/2" sqrt(2)/2, "-√3/2" sqrt(3)/2, 1)
plot sin(x)
