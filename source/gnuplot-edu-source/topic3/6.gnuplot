unset border # nekreslit rámeček
set zeroaxis linetype 1 linecolor rgb "black" # zobrazovat osy

# zobrazení vybraných popisků na osách
set xtics axis ("π/4" pi/4, "π/2" pi/2, "3π/4" 3*pi/4, "π" pi,\
                "5π/4" 5*pi/4, "3π/2" 3*pi/2, "7π/4" 7*pi/4, "2π" 2*pi)
set ytics axis (-1, -0.5, 0, 0.5, 1)

set xrange [-1:2*pi] # nastavení rozsahu na ose x
set parametric # zapnutí parametrického režimu
set size ratio 2.0/(2*pi+1) # nastavení poměru výšky a šírky grafu

set trange [-1:2*pi] # nastavení intervalu hodnot parametru t

y = -0.5 # hodnota pravé strany rovnice
# vypočtení řešení x0 a x1 z [0,2π]
if (y >= 0) {
    x0 = asin(y)
    x1 = pi - asin(y)
} else {
    x0 = 1 * pi - asin(y)
    x1 = 2 * pi + asin(y)
}

# vyznačení průsečíků v grafu funkce sin(x)
set object 1 circle at x0, y size 0.03 front fc rgb "red" fillstyle solid 1.0
set object 2 circle at x1, y size 0.03 front fc rgb "red" fillstyle solid 1.0
# ručně doplněný průsečík pro y=-0.5 v záporné části grafu
set object 3 circle at asin(y), y size 0.03 front fc rgb "red" fillstyle solid 1.0

# úhlové řešení na jednotkové kružnici
set arrow from 0,0 to cos(x0), y nohead ls 10 lc rgb "#ffaa70"
set arrow from 0,0 to cos(x1), y nohead ls 10 lc rgb "#ffaa70"
set object 10 circle at 0,0 radius 0.25 arc[0:x0/(2*pi)*360] fs empty fc rgb "#ffaa70"
set object 11 circle at 0,0 radius 0.35 arc[0:x1/(2*pi)*360] fs empty fc rgb "#ffaa70"

# vyznačení průsečíků na jednotkové kružnici
set object 4 circle at cos(x0), y size 0.03 front fc rgb "#ffaa70" fillstyle solid 1.0
set object 5 circle at cos(x1), y size 0.03 front fc rgb "#ffaa70" fillstyle solid 1.0

# souřadnice průsečíků pro y=-0.5
ly = 0.15
set label "[7π/6; -0.5]" at pi*7/6,-0.5 - ly right
set label "[11π/6;\n-0.5]" at pi*11/6,-0.5 - ly

plot cos(t), sin(t) notitle, \
     [0:2*pi] t, sin(t) title "sin(x)", \
     [-1:2*pi] t,y title sprintf("%g", y)