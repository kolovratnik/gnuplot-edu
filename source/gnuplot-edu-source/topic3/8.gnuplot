set zeroaxis linetype 1 linecolor rgb "black" # zobrazovat osy
set multiplot title "Rozbor řešení soustavy 2 lineárních rovnic o 2 neznámých" \
    layout 2,2 # kreslit 2 řádky a 2 sloupce grafů do jednoho obrázku

set grid back lc rgb'#808080' lt 0 lw 1 # mřížka


set title "Právě jedno řešení"
plot -x-3, -2*x+5


set title "Detail řešení"
set xtics 0.1 format "%0.1f" # nastavení kroku a formátu popisků osy x
set ytics 0.2 format "%0.1f" # a osy y
plot -x-3, -2*x+5, '-' with points pointtype 2 linecolor rgb "red" notitle
8 -11
e


set title "Žádné řešení"
set xtics autofreq format "% h" # návrat k výchozímu nastavení popisků na osách
set ytics autofreq format "% h"
plot -x-3, -x+5


set title "Nekonečně mnoho řešení"
plot -x-3 dashtype (20,20) lw 2, \
     -x-3 dashtype (10,10) lw 2