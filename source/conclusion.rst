Závěr
=====

Seminární práce poskytuje materiál pro seznámení s programem gnuplot ve výuce. Ve stručnosti popisuje ovládání programu, režimy vstupu, vestavěnou nápovědu a demostruje jeho možnosti ve třech tématech - První graf, Zobrazení datového souboru a Grafické řešení rovnic. Každé téma obsahuje několik příkladů, které zahrnují vše potřebné pro provedení ve výuce, tedy popis, zdrojový kód a pro srovnání vykreslený graf.

Byl kladen důraz na jednoduchost - datový soubor je malý, řešené rovnice snadné. Látky záměrně není mnoho, nepředpokládá se, že by právě gnuplot dostal ve výuce větší prostor. Přesto by mohl přinést jisté zpestření, rozšíření představ o tom, jak může aplikační software vypadat a zkusit si jednoduchý "program", jehož výsledkem je graf.

Program gnuplot je mocný nástroj se spoustou možností. V textu poodhalujeme jen nepatrnou část toho, co nabízí. Z uvedených příkladů je vidět, že vzhled grafu lze ve srovnání s výchozím zobrazením podstatně vylepšit dostupnými nastaveními - někdy přímočaře, někdy trikově.

Text je formátován pomocí značkovacího jazyka reStructuredText, z nějž se pomocí projektu Sphinx generuje snadno dostupná online HTML verze a XeLaTeXový zdrojový text pro překlad do PDF. Pomocí autorova rozšíření jsou zobrazeny zdrojové texty příkladů a z nich gnuplotem vykreslené obrázky grafů. XeLaTeXový zdrojový text je před překladem do PDF upraven, aby formát odpovídal požadavkům na závěrečnou práci.
