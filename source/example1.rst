Téma: první graf
================

Vykreslování grafů funkcí je v programu gnuplot velice snadné. Slouží k němu příkaz :keyword:`plot` následovaný předpisem funkce.

Graf lineální funkce :math:`f(x) = x` tedy vykreslíme příkazem

.. xtodo:: gnuplot
  :title: Vykreslený příklad: graf lineární funkce

  plot x

Nezávislá proměnná se označuje x. Její hodnoty jsou vyneseny na vodorovné ose. Ve výchozím nastavení probíhá interval od -10 do 10. Rozsah na vodorovné ose je určen automaticky v závislosti na vykreslované funkci tak, aby byl její graf vykreslen pokud možno celý. Výjimkou jsou neomezené funkce jako například :math:`1/x` nebo :math:`\tg(x)`.

Při kreslení grafu i při interpretaci příkazu bylo využito výchozí nastavení. Podle toho graf vypadá. Výchozí jméno nezávislé proměnné je :math:`x` a :math:`x \in [-10:10]`. Graf má rámeček, legendu, funkční hodnoty jsou vykresleny fialovou barvou, na rámečku jsou vyneseny některé hodnoty os x a y, samotné osy však zobrazeny nejsou.

Jak má graf vypadat bývá určeno pravidly pro vzhled dokumentu, požadavky školy na závěrečnou práci nebo zvyklostmi psaní laboratorního protokolu. Proto následuje příklad ukazující několik z mnoha nastavení. Byl odstaněn rámeček a legenda, nastaven rozsah na ose x, :math:`x \in [0:2\pi]`, doplněn titulek grafu, změněny popisky os a přidány šipky ukazující, kterým směrem hodnoty na osách rostou.

.. xtodo:: gnuplot
  :width: 700
  :title: Vykreslený příklad: osy a graf funkce sin

  #!/usr/bin/gnuplot

  # set encoding utf8
  set key off # nezobrazovat legendu
  set xrange [0:2*pi] # interval na ose x
  set title "Graf funkce sin(x), x∈[0,2π]"
  set zeroaxis linetype 1 linecolor rgb "black" # zobrazovat osy
  # šipka na konci osy x
  set arrow from 2*pi-0.1,0 rto 0.4,0 head size 0.2,25 # délka šipky 0.2, úhel 25°
  set label "x" at 2*pi+0.4, -0.15 # jméno osy x a jeho umístění
  # šipka na konci osy y
  set arrow from 0,1 rto 0,0.3 head size 0.2,25 # délka šipky 0.2, úhel 25°
  set label "y" at -0.3, 1.25 # jméno osy y a jeho umístění
  set rmargin 2*pi+1 # udělat místo pro popisek osy x
  unset border # nekreslit rámeček
  set xtics axis ("π/6" pi/6, "π/4" pi/4, "π/2" pi/2, "3π/4" 3*pi/4, "π" pi,\
                  "5π/4" 5*pi/4, "3π/2" 3*pi/2, "7π/4" 7*pi/4, "2π" 2*pi)
  set ytics axis (-1, "-√3/2" -sqrt(3)/2, "-√2/2" -sqrt(2)/2, -0.5,\
                   0, 0.5, "√2/2" sqrt(2)/2, "-√3/2" sqrt(3)/2, 1)
  plot sin(x)

Většina nastavení je přímočarých, snadno se použije v souladu s dokumentací. Problematické je zobrazení šipek na osách a jejich označení x a y, jelikož je třeba odhadnout a vyzkoušet jejich souřadnice. Tyto prvky se umísťují do souřadného systému grafu, jejich souřadnice tedy záleží na počátku a rozsazích na osách.

Program gnuplot zná nastavení set xlabel {\\"<label>\\"} pro zobrazení popisku osy s možností posunout jej. Výchozí umístění je pro osu x uprostřed pod ní. Posun se provádí relativně k výchozí pozici. Tato možnost se nám pro umístění popisku vpravo od osy neosvědčila.
