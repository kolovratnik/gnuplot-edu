O programu gnuplot
==================

Instalace
*********

Program gnuplot je k dispozici volně ke stažení z Internetu jako freeware. Rozcestník_ s odkazy k různým archivům je na adrese http://gnuplot.info/download.html.

Program lze instalovat překladem zdrojových kódů, které jsou k dispozici, jedná se o OpenSource. Mnoho distribucí včetně Debianu_ a Fedory_ jej nabízí jako balíček. K dispozici jsou také připravené archivy a instalátory pro Windows.

Práci píši pomocí gnuplotu ve verzi 5.2 nainstalovaného v `Debianu Buster`_. Úspěšně jsem vyzkoušel instalaci ve Windows 7 pomocí instalátoru dostupném ze stránky http://www.tatsuromatsuoka.com/gnuplot/Eng/winbin/. Na téže stránce jsou připraveny archivy s 32 i 64 bitovými instalátory a archivy se soubory potřebnými k přímému spuštění programu bez instalace. Během psaní práce stránka přestala fungovat. Instalátory jsou nově k dispozici na adrese http://tmacchant3.starfree.jp/gnuplot/Eng/winbin/.

Jednotlivé způsoby instalace blíže nepopisuji. Půjde-li vše dobře, postupuje se, jak je při instalaci daným způsobem obvyklé. Specifické problémy nelze předjímat.

.. _Debianu Buster: https://www.debian.org/releases/buster/
.. _Rozcestník: http://gnuplot.info/download.html
.. _Debianu: https://www.debian.org/
.. _Fedory: https://getfedora.org/

Dokumentace
***********

K programu je k dispozici přímo na jeho stránkách `obsáhlá dokumentace v angličtině`_ (vyšla i knižně :cite:`9781680921717`). V anglickém jazyce vyšlo i několik knih. O verzi 5 píší například :cite:`Janert2016` a :cite:`Phillips2018`. V knize :cite:`9781849517249` pracující s verzí 4.4 je sbírka řešení mnoha praktických problémů.

gnuplot
nástroj pro kreslení grafů
Katedra optiky, Přírodovědecká fakulta
Univerzita Palackého v Olomouci
http://optics.upol.cz/userfiles/file/GNUPLOT_6.pdf

Program komunikuje v angličtině.

.. _obsáhlá dokumentace v angličtině: http://gnuplot.info/docs_5.2/Gnuplot_5.2.pdf
.. bibliography:: references.bib
   :style: unsrt


Opora v Rámcovém vzdělávacím programu
*************************************

Zapojením gnuplotu lze sledovat cíle Rámcového vzdělávacího programu pro gymnázia [RVPG]_, a to vedení žáků *k využití pokročilejších funkcí k efektivnímu zpracování informací* tím, že tvorbu grafů lze gnuplotem automatizovat a že lze opakovaně využívat jednou již vyzkoušená nastavení vlastností grafu. Dále přispívá k *porozumění zásadám ovládání jednotlivých skupin aplikačního programového vybavení* tím, že představuje zástupce velké skupiny programů ovládaných či ovládatelných bez grafického uživatelského prostření. Výhodou je, že graf přeci jen lze zobrazit do okna s interaktivními ovládacími prvky. Krok od programů s grafickým uživatelským prostředím tedy není tak velký.
Obecně lze říci, že gnuplot je *aplikační software pro práci s informacemi*, kontrétně program pro *modelování a simulace, export a import dat.* Vytráření grafů také podporuje *prezentaci výsledků práce*. Bude-li použit gnuplot, půjde patrně o *prezentaci s využitím pokročilých funkcí aplikačního softwaru.* Některé podporované výstupní formáty jsou dokonce vhodné pro *prezentaci na Internetu.*


.. [RVPG] Rámcový vzdělávací program pro gymnázia. [online]. Praha: Výzkumný ústav pedagogický v Praze, 2007. [cit. 2017-12-17]. Dostupné z WWW:<http://www.nuv.cz/file/159_1_1/>.

Forma dokumentu
***************

Dokument slouží jako seminární práce a musí splňovat požadavky na ní kladené. Existuje tedy `pdf verze`_ (`generovaný XeLaTeXový zdrojový kód`_). Aby mohl prakticky sloužit jako výukový materiál, má také `online podobu`_. Zdrojové kódy příkladů jsou v `archivu`_.

.. _generovaný XeLaTeXový zdrojový kód: gnuplot-edu-xelatex-src.tar.gz
.. _online podobu: https://kolovratnik.gitlab.io/gnuplot-edu/
.. _pdf verze: gnuplot-edu.pdf
.. _archivu: gnuplot-edu-source.tar.gz

Historie
********

V době psaní tohoto textu byla jako nejnovější k dispozici verze programu gnuplot 5.2.8 vydaná v prosinci 2019.
