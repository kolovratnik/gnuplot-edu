Téma: zobrazení datového souboru
================================

Zobrazení dat předvedeme na dvou snadných příkladech.

Délka měsíců v roce
-------------------

Délky měsíců volíme proto, že se jedná o jednoduchá malá data všem dobře známá. Použijeme soubor :download:`month.txt <month.txt>` připravený podle kalendáře. Soubor obsahuje počty dní v jednotlivých měsících v nepřestupném roce, vždy jedno číslo samostatně na řádku.

.. include:: month.txt
   :literal:

Vykreslení datového souboru s výchozím nastavením je velice snadné. Místo funkčního předpisu se do uvozovek uvede cesta k souboru.

.. xtodo:: gnuplot
  :title: Vykreslený příklad: zobrazený datový soubor bez jakéhokoli nastavení

  plot "source/month.txt"

Rozsah na obou osách byl určen automaticky, délka února je zobrazena jako čárka na spodním okraji rámečku, body 31 dní dlouhých měsíců se ztrácejí na horním okraji. Hodnoty :math:`(y_i)_{i=0}` byly zobrazeny jako body :math:`([i, y_i])_{i=0}^{11}`. Graf je nepřehledný, zobrazení desetinných čísel na ose y nedává vzhledem k vizualizované skutečnosti valný smysl.

Několika nastaveními lze graf výrazně vylepšit. Místo bodového zvolíme sloupcový graf. Způsob vykreslení, či styl v terminologii gnuplotu, se uvádí v příkazu plot klíčovým slovem with. Dokumentace (help plot with) uvádí 21 různých stylů. Výchozí bodový styl se nazývá points, sloupcový graf získáme stylem boxes.

Dále upravíme šířku a vzhled sloupců. Výchozí šířka sloupce 1 znamená, že se sloupce dotýkají. Nastavíme poloviční šířku a vyplnění barvou. Jinak by byly vykresleny jen jejich obrysy.

Rozsah na ose x je zvolen tak, aby mezi sloupci a rámečkem vlevo a vpravo zbylo volné místo, ale popisky nezačínaly už 0 a skončily před 13. Aby se zobrazila i lichá čísla, bylo ještě třeba zvolit krok popisků 1. Na ose y je situace zajímavější. Lze zvolit rozsah :math:`[0, 32]`, aby bylo patrné kolísání počtu dní v měřítku délky měsíců, nebo :math:`[27, 32]` pro zvýraznění samotných rozdílů. První variantu ukážeme později, pro následující příklad volíme druhou možnost.

Ještě zbývá vysvětlit klíčové slovo using v příkazu plot. Slouží k výběru sloupců z datového souboru pro zobrazení v grafu. Řekněme, že na kažném řádku záznamy o jednom vzorku (třeba členského státu Evropské unie) či pozorování, a ta jsou popsána hodnotami několika veličin (rozloha, počet obyvatel, průměr ročních srážek). Právě pomocí klíčového slova using vybereme, který sloupec chceme vynést do grafu. Sloupců lze vybrat více. Třeba pro určení souřadnic bodů x a y budou potřeba dva sloupce. V zápisu se oddělí dvojtečkou, pro druhý a třetí sloupec uvedeme tedy using 2:3.

V našem příkladě datový soubor obsahuje jen jeden sloupec, s čímž by si gnuplot poradil sám. Sloupce by však očísloval od 0. Protože se měsíce číslují od 1, potřebujeme k číslu záznamu v proměnné $0 přičítat jedničku. Jelikož se jedná o výraz, jenž chceme vyhodnotit, je zapsán v závorkách. Výšku sloupce určuje hodnota v prvním sloupci datového souboru.

.. xtodo:: gnuplot
  :title: Vykreslený příklad: sloupcový graf

  set title "Počet dní v měsíci"
  set boxwidth 0.5 # nastavení šířky sloupce
  set style fill solid # vyplnění sloupců barvou
  set xrange [0.25:12.75] # interval na ose x
  set yrange [27:31.9] # interval na ose y
  set xtics 1 # popisky na ose x mají krok 1
  set ytics 1 # popisky na ose y mají krok 1
  plot "source/month.txt" using ($0+1):1 title "dny" with boxes

Jména měsíců na ose x
---------------------

V posledním příkladu jsme ukázali, jak dopočítat číslo měsíce z pořadí v datovém souboru. Mohli bychom však potřebovat zobrazit jako popisky jména měsíců. Ukážeme si dva způsoby, jak toho dosáhnout. Jeden je univerzální. Spočívá v tom, že jména měsíců uložíme jako sloupec do datového souboru a řekneme gnuplotu, aby jej použil jako popisky na ose x. Taktéž bychom mohli zobrazit třeba jména planet v grafu jejich velikostí. Druhý způsob je specifický pro jména mesíců a využívá formátovací řetězce.

Začneme druhým způsobem, protože vyžaduje minimum změn proti předchozímu příkladu. Pro formátování popisků na ose x zvolíme režim data a času a formátovacím řetězcem "%B" určíme tisk popisku jako jméno měsíce. Hodnota je stále vypočítávána na základě proměnné $0. Vstupem pro formátování času je počet sekund od počátku Unixové epochy. Proto pořadí záznamu $0 násobíme počtem sekund v 31 dnech. S každým dalším sloupečekm se trefíme do následujícího měsíce.

.. code-block:: gnuplot

  set xdata time # popisky na ose x jsou formátovány jako datum/čas
  set xtics format "%B" # volba formátu - %B je jméno měsíce
  plot "/tmp/monthname.txt" using (3600*24*31*$0):2

Je to trikové řešení. Jména měsíců dostaneme patrně v angličtině. Do češtiny lze přepnout nastavením českého locale. Ani tak nemusíme být spokojeni, jelikož se může stát, že jména budou v genitivu (ve 2. pádě).

.. code-block:: gnuplot

  set locale "cs_CZ.UTF-8"

Nyní se vrátíme k univerzálnějšímu způsobu uvedení popisků do souboru. Soubor se jmény měsíců :download:`monthname.txt <monthname.txt>` by mohl vypadat takto:

.. include:: monthname.txt
   :literal:

Pro přehlednost uvedeme celý popis grafu. Nastavení rozsahu na ose x již není potřeba. V příkazu plot se změnil parametr klíčového slova using. Hodnoty jsou ve druhém sloupci, proto je místo 1 číslo 2. V prvním sloupci jsou popisky, což gnuplotu říkáme za dvojtečkou pomocí funkce xticlabel(1).

.. code-block:: gnuplot

  set title "Počet dní v měsíci"
  set boxwidth 0.5 # nastavení šířky sloupce
  set style fill solid # vyplnění sloupců barvou
  set yrange [27:31.9] # interval na ose y
  set ytics 1 # popisky na ose y mají krok 1
  plot "monthname.txt" using 2:xticlabel(1) title "dny" with boxes


Funkce jako sloupcový graf
---------------------------

Graf počtu dní v jednotlivých měsících vykreslíme ještě jednou. Jednak, jak bylo přislíbeno výše, ukážeme kolísání počtu dní v měřítku délky měsíců a jednak místo vstupu ze souboru vypočteme délky měsíců výrazem. Navíc využijeme další vlastnosti příkazu plot a budeme definovat funkci jedné proměnné.

Definice funkce přímočaře vychází ze zápisu obvyklého v matematice :math:`f(x)=\text{výraz}`. Naše funkce se jmenuje days a je parametrizována proměnnou :math:`month\in \mathbb{Z}, 0<=month<=11`. Symbol procenta označuje zbytek po dělení. Symbol dvojitého rovná se je operace porovnání, v případě rovnosti je výsledkem 1, jinak 0. Zde je použit pro ošetření jednoho zvláštního případu, tedy února, který má v nepřestupném roce o dva dny méně, než by jinak vypočítal výraz. Při číslování měsíců od 0 má únor číslo 1. Tuto funkci chceme vyhodnotit ve 12 bodech, což gnuplotu sdělíme nastavením parametru sample.

Ukážeme si dvě nové vlastnosti příkazu plot. Jednak vykreslení více funkcí do jednoho grafu a jednak volitelné nastavení intervalu, na kterém má funkce být vykreslena. Interval :math:`[a,b]` se zapíše před funkci do hranatých závorek [a:b]. Funkce či datové soubory k vykreslení se zapíší za přikaz plot oddělené čárkou. Kombinace obého využíváme k vynucení rozsahu na osách. Kreslíme konstantní funkci 0 na intervalu :math:`[0.25,12.75]` a funkci days na intervalu :math:`[1,12]`. Jelikož v grafu chceme mít měsíce číslované od 1, odečítáme v argumentu 1. Jelikož zbytek po dělení je definován jen pro celá čísla, používáme ještě funkci int.

.. xtodo:: gnuplot
  :title: Vykreslený příklad: diskrétní funkce jako sloupcový graf

  set key off # nezobrazovat legendu
  set sample 12 # funkci vzorkovat ve 12 bodech
  set boxwidth 0.5 # nastavení šířky sloupce
  set style fill solid # vyplnění sloupců barvou
  set xtics 1 # popisky na ose x mají krok 1
  days(month) = 31 - month % 7 % 2 - 2 * (month == 1)
  plot [0.25:12.75] 0, [1:12] days(int(x) - 1) title "dny" with boxes

Tip na cvičení
--------------

Uvedené příklady lze variovat – měnit či vynechávat jednotlivá nastavení, rozsahy a popisky na osách, upravovat vzhled grafu či barvu a šířku sloupců. Lze zobrazit vlastnosti planet sluneční soustavy, zemí EU, počet obyvatel 10 největších českých měst a podobně.
