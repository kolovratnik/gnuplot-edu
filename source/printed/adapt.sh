#!/bin/bash

MYDIR=$(dirname $(readlink -f "$0"))

sed -f "$MYDIR/adapt-10.sed" -e '# insert title pages
/^\\begin{document}$/,/^\\pagestyle{normal}$/{
    /^\\begin{document}$/{
        r '"$MYDIR/adapt-part-10.tex"'
	b
    }
    s/.*//;
}' | sed -e '# pdfcreationdate
    s/^\\edef\\pdfcreationdate$/&{\\detokenize{D:'$(date +%Y%m%d%H%M%S%z | sed 's/\(..\)$/'\''\1/')'}}/
'
