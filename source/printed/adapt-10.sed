#################################
# delete unnecessary definition #
#################################

/^\\def\\sphinxdocclass{book}/d


#######################################
# set document class, usepackage pdfx #
# and set continuous figure numbering #
#######################################

/^\\documentclass/{
    s/sphinxmanual/book/
    a\\\edef\\pdfcreationdate\
\\usepackage[a-2u]{pdfx}\
\\usepackage{chngcntr}\
\\counterwithout{figure}{chapter}
}


########################
# make foot less fancy #
########################

/^\\usepackage{sphinxmessages}$/{
a\
\
\\usepackage{fancyhdr}\
\\fancypagestyle{plain}{%\
    \\fancyhf{} % clear all header and footer fields\
    \\fancyfoot[C]{\\thepage} % except the center\
    \\renewcommand{\\headrulewidth}{0pt}\
    \\renewcommand{\\footrulewidth}{0pt}\
}\
\

}


########################
# delete unwanted text #
########################

/^Obsah:$/,+2d


##############################
# do not number Introduction #
##############################

/^\\chapter{Úvod}/{
    s/^\\chapter{Úvod}/\\chapter*{Úvod}/
    a\\\addcontentsline{toc}{chapter}{Úvod}
}


#####################
# fix font settings #
#####################

##  not needed for build in gitlab

##  /\\setmainfont/,/^$/{
##  s/..*//; t x
##  i\\\setmainfont[\
##    Extension      = .ttf,\
##    UprightFont    = *,\
##    ItalicFont     = *Italic,\
##    BoldFont       = *Bold,\
##    BoldItalicFont = *BoldItalic\
##  ]{FreeSerif}\
##  \\setsansfont[\
##    Extension      = .ttf,\
##    UprightFont    = *,\
##    ItalicFont     = *Oblique,\
##    BoldFont       = *Bold,\
##    BoldItalicFont = *BoldOblique\
##  ]{FreeSans}\
##  \\setmonofont[\
##    Extension      = .ttf,\
##    UprightFont    = *,\
##    ItalicFont     = *Oblique,\
##    BoldFont       = *Bold,\
##    BoldItalicFont = *BoldOblique\
##  ]{FreeMono}
##  b
##  :x D
##  }


##########################
# make chapter lessfancy #
##########################

/^\\begin{document}$/{ i\
\\titleformat{\\chapter}\
{\\normalfont\\huge\\bfseries}{\\thechapter.}{20pt}{\\Huge}\
\
\\titleformat{\\section}\
{\\normalfont\\Large\\bfseries}{\\thesection.}{1em}{}\
\
\\titleformat{\\subsection}\
{\\normalfont\\large\\bfseries}{\\thesubsection.}{1em}{}\
\
\\titleformat{\\subsubsection}\
{\\normalfont\\normalsize\\bfseries}{\\thesubsubsection.}{1em}{}\
\

}


####################
# non break spaces #
####################

/^[^%]/ {
    s/\<\([aikosuvz]\) /\1~/g
}
/\\begin{sphinxVerbatim}/,/\\end{sphinxVerbatim}/{ s/~/ /g }
