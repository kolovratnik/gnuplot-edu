Návrhy výukových témat
======================

Následující témata jsou myšlena jako materiál pro přípravu na představení gnuplotu ve výuce. Demonstrují základní rysy práce s gnuplotem. Jedná se o úzký výběr, který by mohl tvořit náplň jedné až dvou výukových hodin informatiky.

Příklady jsou uvedeny popisem toho, co a jak chceme zobrazit. Následuje zdrojový text grafu, tedy konkrétní realizace toho, co bylo slíbeno v popisu, a končí vykresleným grafem.

Téma: první graf
----------------

V kapitole první graf předvedeme vykreslení lineární funkce s výchozím nastavením vlastností grafu a některá tato výchozí nastavení zmiňujeme. Následuje příklad s funkcí sinus, v němž ukazujeme, jak udělat vlastní popisky os, jak zobrazit šipky na jejich koncích a umístit jejich názvy, jak vypnout legendu a jak vykreslit graf bez rámečku. Graf se blíží autorovým vzpomínkám na grafy na střední škole.

Téma: zobrazení datového souboru
--------------------------------

V tématu zobrazení datového souboru ukazujeme na jednoduchém příkladu, jak vytvořit graf z posloupnosti hodnot. Nejprve zobrazíme bodový graf bez úprav jeho vlastností a výsledek popíšeme. Data zobrazíme ještě jako sloupcový graf s využitím nastavení pro lepší vzhled. Následují návrhy, jak zobrazit jako popisky sloupců jména měsíců. Nakonec pro zajímavost vykreslíme do sloupcového grafu diskrétní funkci.

Téma: grafické řešení rovnic
----------------------------

Na tématu grafického řešení rovnic ukazujeme, jak zobrazit několik funkcí v jednom grafu a také několik grafů v jednom obrázku. Dvěma různými způsoby vyznačujeme průsečíky průběhů funkcí a popisujeme jejich souřadnice. K popisu grafu řešícího jednoduchou goniometrickou rovnici využijeme parametrizaci. Dále řešíme soustavu dvou lineárních rovnic o dvou neznámých. Provádíme rozbor různých případů a upozorňujeme na některé speciální případy a omezení grafické metody. Zobrazujeme grafy s mřížkou.
