FROM debian:buster-slim
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install \
        gnuplot-nox \
        python3 python3-pip \
        dvipng texlive-xetex fonts-freefont-otf latexmk
RUN pip3 install -U sphinx sphinxcontrib-bibtex
